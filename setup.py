from setuptools import setup, find_packages

setup(
    name='chimerautils',
    packages=find_packages(exclude=['tests']),
    install_requires=['networkx'],
    license='',
    author='',
    author_email='',
    description=''
)
